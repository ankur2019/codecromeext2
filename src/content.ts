window.onload = () => {
    const button = document.createElement('button');
    button.id = "darkModeButton";
    button.textContent = chrome.i18n.getMessage("enableDarkModeText");
    // internationalisation

    const input = document.createElement('input');
    input.type = 'checkbox';
    input.id = 'darkSetting';

    document.querySelector("#voice-search-button").prepend(button, input, 'Auto apply?');
    button.addEventListener('click', () => enableDarkMode());
    input.addEventListener('click', () => storeSetting());

    checkSetting();
}

function checkSetting() {
    chrome.storage.local.get(['enabled', 'color'], (result) => {
        const isEnabled = result.enabled;
        const color = result.color;
        const settingCheckbox = document.getElementById('darkSetting') as HTMLInputElement;
        settingCheckbox.checked = isEnabled;

        if (isEnabled) {
            enableDarkMode();
        }
    });
}

function storeSetting() {
    const settingCheckbox = document.getElementById('darkSetting') as HTMLInputElement;
    const isEnabled = settingCheckbox.checked;
    const setting = { enabled: isEnabled, color: 'purple' };
    chrome.storage.local.set(setting, () => {
        console.log("stored", setting);
    })
}

function enableDarkMode() {
    const websiteBody = document.getElementsByTagName('ytd-app')[0] as HTMLElement;
    websiteBody.style.backgroundColor = 'black';
}